Pour tester le code lié aux interfaces il est nécessaire d'installer JavaFX car il n'est plus inclus par defaut dans le JDK
https://gluonhq.com/products/javafx/

/!\ IL FAUT AUSSI CONFIGURER L'IDE POUR CREER UN PROJET JAVAFX ET NON JAVA SINON LES IMPORT NE FONCTIONNE PAS /!\